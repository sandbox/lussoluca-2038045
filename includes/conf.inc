<?php

/**
 * @file
 * Conf class is use to manage environment settings.
 *
 */


/**
 * The production environment.
 */
define('PROD', 3);

/**
 * The stage environment.
 */
define('STAGE', 2);

/**
 * The development environment.
 */
define('DEV', 1);

/**
 * The local environment.
 */
define('LOCAL', 0);

/**
 *
 */
class Conf {

  /**
   *
   */
  public static function getEnvironmentSettings($env = NULL) {
    $settings = array(
      'position' => 'right',
      'margin' => 30,
    );

    switch ($env) {
      case PROD:
        $settings += array(
          'text' => t('Production environment'),
          'color' => 'red',
        );
        break;
      case STAGE:
        $settings += array(
          'text' => t('Stage environment'),
          'color' => 'orange',
        );
        break;
      case DEV:
        $settings += array(
          'text' => t('Dev environment'),
          'color' => 'yellow',
        );
        break;
      case LOCAL:
        $settings += array(
          'text' => t('Local environment'),
          'color' => 'white',
        );
        break;
    }

    return $settings;
  }

}
