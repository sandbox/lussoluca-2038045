Installation
------------
Conf can be installed like any other Drupal module -- place it in the
modules directory for your site and enable it on the admin/build/modules page.

Configuration
-------------

Insert the configuration into your settings.php:

<pre><code>include_once('./sites/all/modules/conf/includes/conf.inc');
$conf = array(
  'environment' => LOCAL,
  'modules_to_enable' => array('smtp', 'varnish', 'memcache'),
  'modules_to_disable' => array('devel', 'coder', 'views_ui'),
  'mail' => array(
    'override_to' => 'email@example.it',
    'debug' => TRUE,
  ),
);
</code></pre>

where:

- environment: LOCAL, DEV, STAGE or PROD
- modules_to_enable: a comma separated list of modules to enable
- modules_to_disable: a comma separated list of modules to disable
- mail: override the TO field for all mail sent from the site (if debug is TRUE adds a new header, X-Original-To, with the original TO address)

Enabling and disabling modules are done on each cache clear.

Environment indication is based on [Environment indicator][1] module

[1]: http://drupal.org/project/environment_indicator
